.include "book_utils.asm"

.text
.globl replaceBook

replaceBook:
	addi	$sp,$sp,-8
	sw	$ra,0($sp)
	sw	$s0,4($sp)
	
	move	$t0, $a0
	
	# Inizializzazione del nuovo nodo
	sw	$zero, book_next($t0)		# NULL
	
	printString(MSG_INPUT_TITLE)
	getStringAddress (book_title, $t0)
	jal	trimString			# '\n' -> '\0' //LA MACRO HA GIÀ MESSO L'INDIRIZZO DELLA STRINGA IN a0

	printString(MSG_INPUT_AUTHOR)
	getStringAddress (book_author, $t0)
	jal	trimString			# '\n' -> '\0' //LA MACRO HA GIÀ MESSO L'INDIRIZZO DELLA STRINGA IN a0

	printString(MSG_INPUT_PUBLISHER)
	getStringAddress (book_publisher, $t0)
	jal	trimString			# '\n' -> '\0' //LA MACRO HA GIÀ MESSO L'INDIRIZZO DELLA STRINGA IN a0

	printString(MSG_INPUT_GENRE)
	getStringAddress (book_genre, $t0)
	jal	trimString			# '\n' -> '\0' //LA MACRO HA GIÀ MESSO L'INDIRIZZO DELLA STRINGA IN a0
	
	
    	sw	$zero, book_validity($t0)	# Messo valido di default
	
	move	$v0, $t0			# Reinserisco come valore di ritorno l'indirizzo della struttura, che andrà usato per l'inserimento

replaceBook_exit:
	lw	$s0, 4($sp)
	lw	$ra, 0($sp)
	addi	$sp, $sp, 8
	jr	$ra

.include "book_utils.asm"

.text
.globl strcmp

# String Compare: ripoff della medesima funzione della libreria standard
strcmp:
	addi	$sp, $sp, -4
	sw	$ra,0($sp)

strcmp_loop:
	lb	$t0, 0($a0)		# prendo il carattere della Striga S
	lb	$t1, 0($a1)		# prendo il carattere della Striga T

	sub	$v0, $t0, $t1		# li confronto
	bnez	$v0, strcmp_finish		# non corrispondono? salto alla conclusione

	addi	$a0, $a0, 1		# punto al successivo carattere di S
	addi	$a1, $a1, 1		# punto al successivo carattere di T

	bnez $t0, strcmp_loop		# Se non arrivato a fine stringa, vado avanti

strcmp_finish:	
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	jr $ra				# return

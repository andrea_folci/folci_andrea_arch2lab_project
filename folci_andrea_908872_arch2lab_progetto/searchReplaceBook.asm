.include "book_utils.asm"

.text
.globl srcReplaceBook

srcReplaceBook:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	
	
	# setup per il loop
	li	$t0,0					# prev = NULL
	move	$t1, $s0				# cur = list_head
	j	srcReplaceBook_test

srcReplaceBook_loop:
	lw	$t3, book_validity($t1)			# Ottengo indirizzo stringa candidata
	bnez	$t3, srcReplaceBook_now			# se si, fornisco il rimpiazzo

	move	$t0, $t1				# prev = cur
	lw	$t1, book_next($t1)			# cur = cur->node_next

srcReplaceBook_test:
	bnez	$t1, srcReplaceBook_loop

srcReplaceBook_Failure:
	li	$v0, -1
	j	srcReplaceBook_exit
	
srcReplaceBook_now:
	lw	$t3, book_next($t1)
	beqz	$t0, srcReplaceBook_front		# prev == NULL? if yes, fly
	sw	$t3, book_next($t0)
	move	$v0, $t1				# Metto l'indirizzo della stuttura da riutilizzare nel registro output v0
	j	srcReplaceBook_exit

srcReplaceBook_front:
	move	$s0, $t3	
	move	$v0, $t1				# Metto l'indirizzo della stuttura da riutilizzare nel registro output v0
	
srcReplaceBook_exit:
	lw	$ra, 0($sp)
 	addi	$sp, $sp, 4
	jr	$ra

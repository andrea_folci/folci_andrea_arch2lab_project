.include "book_utils.asm"

.data
	.align 2
	MSG_SUCCESS:		.asciiz		"Libro eliminato dall'archivio.\n"
	.align 2
	MSG_FAILURE:		.asciiz		"Libro non trovato, nulla è stato eliminato.\n"

.text
.globl eraseBook

# SEARCH BOOK:	La funzione prende in input un titolo (Parametro a0) e lo cerca nella lista

eraseBook:
	addi	$sp, $sp, -8
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	
	# setup per il loop
	li	$t0,0				# prev = NULL
	move	$t1, $s0			# cur = list_head
	move	$t2, $a0			# Target in t2
	j	eraseBook_test

eraseBook_loop:
	lw	$a1, book_title($t1)		# Ottengo indirizzo stringa candidata
	lw	$t3, book_validity($t1)
	bnez	$t3, eraseBook_invalid
	
	move	$a0, $t2
	addi	$sp, $sp, -8
	sw	$t0, 0($sp)
	sw	$t1, 4($sp)
		
	jal	strcmp				# candidata = target?
	
	lw	$t1, 4($sp)
	lw	$t0, 0($sp)
 	addi	$sp, $sp, 8
 	
	beqz	$v0, eraseBook_now		# se si, stampo

eraseBook_invalid:
	move	$t0, $t1			# prev = cur
	lw	$t1, book_next($t1)		# cur = cur->node_next

eraseBook_test:
	bnez	$t1, eraseBook_loop
	beqz	$t1, eraseBook_Failure		# Raggiunta la fine della lista senza cancellare
	
eraseBook_now:
	li 	$t3, 1
	sw	$t3, book_validity($t1)
	printString(MSG_SUCCESS)
	j	eraseBook_exit
		
eraseBook_Failure:
	printString(MSG_FAILURE)
	j	eraseBook_exit

eraseBook_exit:

	lw	$s0, 4($sp)
	lw	$ra, 0($sp)
 	addi	$sp, $sp, 8
	jr	$ra

.include "book_utils.asm"

# Costanti e Variabili statiche
	.data
		.align 2
		INTRODUCTION_MSG:	.asciiz	"\n\nBENVENUTO NEL SISTEMA DI GESTIONE DELLA BIBLIOTECA DI ALBAIRATE E VERMEZZO.\nSCEGLI SE:\n1 - Inserire un nuovo volume\n2 - Stampare tutti i volumi presenti nell'archivio\n3 - Cercare i dettagli di uno specifico volume\n4 - Eliminare un libro dall'archivio\n9 - Uscire dal sistema\n\nSCELTA: "
		.align 2
		ERR_MSG:		.asciiz "È STATO INSERITO UN INPUT IMPREVISTO, RIPROVARE\n\n\n"
		
		.align 2
		ADD_MSG:		.asciiz "AGGIUNTA DI UN LIBRO:\n\n"
		.align 2
		PRINTLIB_MSG:		.asciiz "STAMPA DELL'ARCHIVIO:\n\n"
		
		.align 2
		SEARCH_MSG:		.asciiz "MENÙ DI RICERCA:\n\n"
		.align 2
		SEARCH_INPUT_MSG:	.asciiz "Inserisci ciò che vuoi cercare: "

				
		.align 2
		ERASE_MSG:		.asciiz "MENÙ DI ELIMINAZIONE:\n\n"
		.align 2
		ERASE_INPUT_MSG:	.asciiz "Inserisci ciò che vuoi eliminare: "
		.align 2
		
		GOODBYE_MSG:		.asciiz "ARRIVEDERCI!\n"
	
		STR_BUFFER:		.word	50

# main
	.globl  main
	
# Codice
	.text

# main:		presento il menù all'utente ed eseguo ripetutamente quanto chiede
#		fino a che non viene inserito il valore di uscita '9'.

main:

	li	$s0, 0				#$s0 = List Head <- NULL

menu_loop:
	printString(INTRODUCTION_MSG)	
	readInt					# SCAN INTEGER VALUE: "SCELTA"
	
## SWITCH CASES JUMPS ##
	beq	$v0, 1, add_book
	beq	$v0, 2, print_archive
	beq	$v0, 3, search_book
	beq	$v0, 4, delete_book	
	
	beq	$v0, 9, main_exit		# CASE 9 - EXIT
## END SWITCH CASES JUMPS ##

## SWITCH CASES LIST ##		
default:
	printString(ERR_MSG)
	j	menu_loop
	
add_book:
	printString(ADD_MSG)			# Print: "AGGIUNTA DI UN LIBRO:"
	jal	srcReplaceBook
	bgez	$v0, replaceOldBook
	
createNewBook:
	jal	newBook
	move	$a0, $v0			# Inserisco il puntatore alla nuova struttura creato da "newBook" nell'argument in input a insertBook
	jal	insertBook
	
	j	menu_loop	
	
replaceOldBook:
	move	$a0, $v0			# Inserisco il puntatore della vecchia struttura fornito da "searchReplaceBook" nell'argument in input a replaceBook
	jal	replaceBook
	move	$a0, $v0
	jal	insertBook
	
	j	menu_loop
	
print_archive:
	printString(PRINTLIB_MSG)		# Print: "STAMPA DELL'ARCHIVIO:"
	move	$a0,$s0
	jal	printLibrary
	printString(STR_NEWLINE)
	j	menu_loop
	
search_book:
	printString(SEARCH_MSG)			# Print: "MENÙ DI RICERCA:"
	printString(SEARCH_INPUT_MSG)
	la	$t1, STR_BUFFER
	getString($t1, 50)
	move	$a0, $t1
	jal	trimString
	
	
	jal	searchBook
	j	menu_loop
	
delete_book:
	printString(ERASE_MSG)			# Print: "MENÙ DI ELIMINAZIONE:"
	printString(ERASE_INPUT_MSG)
	la	$t1, STR_BUFFER
	getString($t1, 50)
	move	$a0, $t1
	jal	trimString
	
	jal	eraseBook
	j	menu_loop
## END SWITCH CASES ##

main_exit:
	printString(GOODBYE_MSG)
	exit

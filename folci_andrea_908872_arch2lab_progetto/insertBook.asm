.include "book_utils.asm"

.text
.globl insertBook

insertBook:
	addi	$sp, $sp, 4
	sw	$ra, 0($sp)
	
	# setup per il loop
	li	$t3,0				# prev = NULL
	move	$t4, $s0			# cur = list_head
	move	$t5, $a0
	j	insert_test

insert_loop:
	lw	$a0, book_title($t5)		# get new string address
	lw	$a1, book_title($t4)		# get current string address
	jal	strcmp				# compare them -- new < cur?
	bltz	$v0, insert_now			# if yes, insert after prev

	move	$t3,$t4				# prev = cur

	lw	$t4, book_next($t4)		# cur = cur->node_next

insert_test:
	bnez	$t4, insert_loop		# cur == NULL? if no, loop

insert_now:
	sw	$t4, book_next($t5)		# new->node_next = cur
	beqz	$t3, insert_front		# prev == NULL? if yes, fly
	sw	$t5, book_next($t3)		# prev->node_next = new
	j	insert_done

insert_front:
	move	$s0, $t5			# list_head = new

insert_done:

	lw	$ra,0($sp)
 	addi	$sp,$sp,4
	jr	$ra

# ----------------   BOOK MACROS   ---------------- #


# pseudo struct "Libro" (Struttura di puntatori a stringa, la memoria per le stringhe viene allocata a parte)
	.eqv	book_next	0
	.eqv	book_title	4
	.eqv	book_author	8
	.eqv	book_publisher	12
	.eqv	book_genre	16
	.eqv	book_validity	20
	.eqv	book_size	24
	
# stringhe ricorrenti nelle funzioni legate a "Libro"
.data
	.align 2
	STR_VOLUME:		.asciiz		"VOLUME: "
	.align 2
	STR_TITLE:		.asciiz		"TITOLO: "
	.align 2
	STR_AUTHOR:		.asciiz		"AUTORE: "
	.align 2
	STR_PUBLISHER:		.asciiz		"EDITORE: "
	.align 2
	STR_GENRE:		.asciiz		"GENERE: "
		
	.align 2
	MSG_INPUT_TITLE:	.asciiz		"Inserisci il Titolo: "
	.align 2
	MSG_INPUT_AUTHOR:	.asciiz		"Inserisci l'Autore: "
	.align 2
	MSG_INPUT_PUBLISHER:	.asciiz		"Inserisci l'Editore: "
	.align 2
	MSG_INPUT_GENRE:	.asciiz		"Inserisci il Genere: "
	
	.align 2
	STR_NEWLINE:		.asciiz		"\n"
	.align 2
	STR_SEPARATOR:		.asciiz		"---"

# ---------------- SYSCALLS MACROS ---------------- #


.eqv PRINT_INT_CODE		1
.eqv PRINT_STRING_CODE		4
.eqv READ_INT_CODE		5
.eqv READ_STRING_CODE		8
.eqv MALLOC_CODE		9
.eqv CLEAN_EXIT_CODE		10

# Print Int: Stampa a schermo l'intero di cui s'è fornito il registro
	.macro printInt (%addr)
		li $v0 PRINT_INT_CODE
		move $a0 %addr
		syscall
	.end_macro

# Read Int: Legge da tastiera un numero intero e lo inserisce in $v0
	.macro readInt
		li $v0 READ_INT_CODE
		syscall
	.end_macro
	

# Print String: Carica in $a0 la stringa di cui s'è passato l'indirizzo con l'etichetta e la stampa.	
	.macro printString (%label)
		li $v0, PRINT_STRING_CODE
		la $a0, %label
		syscall
	.end_macro
	
	.macro printStringAddress (%offset, %address)
		lw $a0, %offset(%address)
		li $v0 PRINT_STRING_CODE
		syscall
	.end_macro
	
	.macro getString (%strReg, %maxStrLen)
		move	$a0, %strReg   				# Zona di memoria dove memorizzare la stringa
		li	$a1, %maxStrLen    			# Massima lunghezza in byte della stringa
		li	$v0 READ_STRING_CODE
		syscall
	.end_macro
	
	.macro getStringAddress (%offset, %address)
		lw $a0, %offset(%address)
		li $v0 READ_STRING_CODE
		syscall
	.end_macro
	
	.macro malloc (%size)
		li $v0 MALLOC_CODE
		li $a0 %size
		syscall
	.end_macro

	.macro exit
		li $v0 CLEAN_EXIT_CODE
		syscall
	.end_macro

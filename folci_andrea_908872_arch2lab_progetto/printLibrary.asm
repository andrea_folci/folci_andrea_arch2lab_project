.include "book_utils.asm"

.text
.globl printLibrary

# Print Library: Scorre la lista dei libri e, se non marcati per l'eliminazione, li stampa a schermo. Si ferma al raggiungimento del puntatore nullo.
printLibrary:
	addi	$sp,$sp,-8
	sw	$ra,0($sp)
	sw	$s0,4($sp)

	beq	$s0, $zero, printLibrary_exit
	addi	$t0, $zero, 1
    
printLibrary_loop:
	lw	$t1, book_validity($s0)
	bnez	$t1, printLibrary_next		# CHECK: se il libro è marcato come eliminato passa al successivo
    
    	printString(STR_NEWLINE)		#
    	printString(STR_SEPARATOR)		# PRINT "\n---\n"
	printString(STR_NEWLINE)		#
    
	printString(STR_VOLUME)			# PRINT "VOLUME: "
	printInt($t0)
	printString(STR_NEWLINE)
    
    
	printString(STR_TITLE)			# PRINT "TITOLO: "
	printStringAddress(book_title, $s0)
	printString(STR_NEWLINE)
    
	printString(STR_AUTHOR)			# PRINT "AUTORE: "
	printStringAddress(book_author, $s0)
	printString(STR_NEWLINE)
    	
	printString(STR_PUBLISHER)		# PRINT "EDITORE: "
	printStringAddress(book_publisher, $s0)
	printString(STR_NEWLINE)
    
	printString(STR_GENRE)			# PRINT "GENERE: "
	printStringAddress(book_genre, $s0)
	printString(STR_NEWLINE)
    
	printString(STR_NEWLINE)		#
	printString(STR_SEPARATOR)		# PRINT "\n---\n"
	printString(STR_NEWLINE)		#
	
	addi $t0, $t0 1
    
printLibrary_next:
	lw	$s0, book_next($s0)		# node = node->node_next
	bnez	$s0, printLibrary_loop

printLibrary_exit:
	lw	$s0, 4($sp)
	lw	$ra, 0($sp)
	addi	$sp, $sp, 8
	jr	$ra

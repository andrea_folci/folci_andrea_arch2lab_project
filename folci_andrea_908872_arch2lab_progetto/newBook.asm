.include "book_utils.asm"

.text
.globl newBook

newBook:
	addi	$sp,$sp,-8
	sw	$ra,0($sp)
	sw	$s0,4($sp)
	
	printString(MSG_INPUT_TITLE)
	jal	allocateNewString
	move	$s1,$v0

	printString(MSG_INPUT_AUTHOR)
	jal	allocateNewString
	move	$s2,$v0

	printString(MSG_INPUT_PUBLISHER)
	jal	allocateNewString
	move	$s3,$v0

	printString(MSG_INPUT_GENRE)
	jal	allocateNewString
	move	$s4,$v0
    
	# Trimming di tutte le stringhe prese in input: il carattere di "A Capo" viene sostituito col Terminatore    
	move	$a0,$s1
	jal	trimString			# '\n' -> '\0'
	move	$a0,$s2
	jal	trimString			# '\n' -> '\0'  
	move	$a0,$s3
	jal	trimString			# '\n' -> '\0'
	move	$a0,$s4
	jal	trimString			# '\n' -> '\0'
    
	# Creazione della nuova struttura libro a cui associare i valori
	malloc(book_size)			# Allocazione dello spazio in memoria per il libro
	move	$s0, $v0	
	
	# Inizializzazione del nuovo nodo
	sw	$zero,book_next($s0)		# NULL
	sw	$s1, book_title($s0)	
	sw	$s2, book_author($s0)	
	sw	$s3, book_publisher($s0)	
	sw	$s4, book_genre($s0)
	sw	$zero, book_validity($s0)	# Messo valido di default
	
newBook_exit:

	lw	$s0, 4($sp)
	lw	$ra, 0($sp)
	addi	$sp, $sp, 8
	jr	$ra

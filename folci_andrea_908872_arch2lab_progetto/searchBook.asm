.include "book_utils.asm"

.data
	.align 2
	MSG_SUCCESS:		.asciiz		"Libro presente nell'archivio, questi sono i dati salvati:\n\n"
	.align 2
	MSG_FAILURE:		.asciiz		"Libro non trovato\n"

.text
.globl searchBook

# SEARCH BOOK:	La funzione prende in input un titolo (Parametro a0) e lo cerca nella lista

searchBook:
	addi	$sp, $sp, -8
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	
	# setup per il loop
	li	$t0,0				# prev = NULL
	move	$t1, $s0			# cur = list_head
	move	$t2, $a0			# Target in t2
	j	searchBook_test

searchBook_loop:
	lw	$a1, book_title($t1)		# Ottengo indirizzo stringa candidata
	lw	$t3, book_validity($t1)
	bnez	$t3, searchBook_invalid
	
	move	$a0, $t2
	addi	$sp, $sp, -8
	sw	$t0, 0($sp)
	sw	$t1, 4($sp)
		
	jal	strcmp				# candidata = target?
	
	lw	$t1, 4($sp)
	lw	$t0, 0($sp)
 	addi	$sp, $sp, 8
 	
	beqz	$v0, searchBook_now		# se si, stampo

searchBook_invalid:
	move	$t0, $t1				# prev = cur
	lw	$t1, book_next($t1)		# cur = cur->node_next

searchBook_test:
	bnez	$t1, searchBook_loop
	beqz	$t1, searchBook_Failure		# Raggiunta la fine della lista senza cancellare
	
searchBook_now:
	printString(MSG_SUCCESS)
	printString(STR_TITLE)
	printStringAddress(book_title, $t1)
	printString(STR_NEWLINE)
    
	printString(STR_AUTHOR)
	printStringAddress(book_author, $t1)
	printString(STR_NEWLINE)
    
	printString(STR_PUBLISHER)
	printStringAddress(book_publisher, $t1)
	printString(STR_NEWLINE)
    
	printString(STR_GENRE)
	printStringAddress(book_genre, $t1)
	printString(STR_NEWLINE)
	j	searchBook_exit
		
searchBook_Failure:
	printString(MSG_FAILURE)
	j	searchBook_exit

searchBook_exit:

	lw	$s0, 4($sp)
	lw	$ra, 0($sp)
 	addi	$sp, $sp, 8
	jr	$ra

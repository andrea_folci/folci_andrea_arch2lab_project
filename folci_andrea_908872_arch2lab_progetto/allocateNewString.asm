.include "book_utils.asm"

.text
.globl allocateNewString

#Read String: si differenzia dal getString perchè si occupa anche della memorizzazione della stringa nello heap.
allocateNewString:
	addi	$sp,$sp,-8
	sw	$ra,0($sp)
	sw	$s0,4($sp)

	malloc(50)
	getString($v0, 50)
	move	$v0,$a0				# restore string address

	lw	$s0,4($sp)
	lw	$ra,0($sp)
	addi	$sp,$sp,8
	jr	$ra
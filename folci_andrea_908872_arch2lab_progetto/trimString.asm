.include "book_utils.asm"

.text
.globl trimString

# String Trimmer: sostituisco l'eventuale carattere di new line "\n" con il terminatore "\0"
trimString:
	addi	$sp, $sp, -12
	sw	$ra, 0($sp)
	sw	$a0, 4($sp)
	sw	$a1, 8($sp)
	
	li	$t6, 0x0A			# 0x0A è l'ASCII per lo \n

trimString_loop:
	lb	$t7, 0($a0)			# copio il carattere corrente della stringa in t1
	beq	$t7, $t6, trimString_replace	# se il carattere equivale al \n, passo alla sostituzione con lo \0
	beqz	$t7, trimString_finish		# se ho raggiunto la fine della stringa senza trovare lo \n, chiudo il loop
	addi	$a0, $a0, 1			# punto al carattere successivo
	j	trimString_loop			# loop

trimString_replace:
	sb	$zero, 0($a0)            # trasformo il carattere di new line nel terminatore

trimString_finish:
	
	lw	$a1, 8($sp)
	lw	$a0, 4($sp)
	lw	$ra, 0($sp)
	addi	$sp, $sp, 12
	jr	$ra			# return

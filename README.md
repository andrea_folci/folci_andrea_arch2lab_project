#Progetto ArchLab II


*Autore:     Andrea Folci  - 908857*

Il progetto consiste nell'implementazione di un software per la gestione di un archivio di una biblioteca.
Il software prenderà in input da console dei volumi, composti da attributi quali il Titolo, l'Autore, l'Editore e il Genere e li inserirà in memoria centrale mantenendone l'ordine lessicografico sulla chiave "Titolo".


## Convenzioni usate nel progetto

Ogni blocco funzionale del progetto è stato scritto in un file a sè stante, per migliorare la leggibilità del programma.

Il file book_utils contiene le Macro delle più comuni syscalls, le stringhe comuni a più parti del programma, e le costanti che definiscono le zone di memoria di ogni parte della struct "Libro".

L'input e l'output avvengono sul'emulazione del terminale di MARS.
